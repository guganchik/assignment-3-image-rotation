#ifndef FILE_H
#define FILE_H

#include <stdbool.h>
#include <stdio.h>

void file_open_read (FILE** f , const char *fname);

bool file_read (void *buffer, size_t size, size_t count, FILE* file, size_t padding);

void file_open_write (FILE** f , const char *fname);

bool file_write (const void *buffer, size_t size, size_t count, FILE* file, size_t padding);



#endif
