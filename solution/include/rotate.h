#ifndef TRANSFORM_H
#define TRANSFORM_H

#include "image.h"

bool image_rotate(struct image *result, const struct image *source);
#endif
