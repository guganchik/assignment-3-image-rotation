#ifndef IMAGE_H
#define IMAGE_H

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

struct pixel {
  uint8_t b, g, r; 

};

struct image {
  uint64_t width, height;
  struct pixel* data;
};

struct image image_create (uint64_t width, uint64_t height);
struct pixel *image_at(const struct image *this, size_t col, size_t row);
void image_delete (struct image img);
#endif
