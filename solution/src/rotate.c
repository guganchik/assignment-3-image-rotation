#include "image.h"
#include "rotate.h"

bool image_rotate(struct image *result, const struct image *source) {

	for (size_t row = 0; row < source->height; ++row) {
		for (size_t col = 0; col < source->width; ++col) {
			size_t new_row = col;
			size_t new_col = source->height - row -1;

			*image_at(result, new_col, new_row) = *image_at(source, col, row);
		}
	}

    return true;
}
