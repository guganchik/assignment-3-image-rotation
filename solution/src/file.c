#include "bmp.h"

void file_open_read (FILE** f , const char *fname) {
    *f = fopen(fname, "rb");
    if (!(*f)) {
        fprintf(stderr, "Couldn't open file '%s'\n", fname);
    }
}

bool file_read (void *buffer, size_t size, size_t count, FILE* file, size_t padding) {
    if ( !fread (buffer, size, count, file) || fseek(file, (long) padding, SEEK_CUR) ) return false;
    return true;
}

void file_open_write (FILE** f , const char *fname) {
    *f = fopen(fname, "wb");
    if (!(*f)) {
        fprintf(stderr, "Couldn't open file '%s'\n", fname);
    }
}

bool file_write (const void *buffer, size_t size, size_t count, FILE* file, size_t padding) {
    // uint8_t *trash = malloc(padding);
    bool write_status = fwrite (buffer, size, count, file);
    fwrite(&padding, padding, 1, file);
    // free(trash);
    return write_status;

}
