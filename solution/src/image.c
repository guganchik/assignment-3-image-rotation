#include <stdlib.h>

#include "image.h"

struct image image_create (uint64_t width, uint64_t height) {
    return (struct image) {
        .height = height,
        .width = width,
        .data = malloc(sizeof(struct pixel) * height * width)
    };
}

struct pixel *image_at(const struct image *const this, size_t col, size_t row) {
	return this->data + col + row * this->width;
}


void image_delete (struct image img) {
    free(img.data);
    img.data = NULL;
}
