#include "bmp.h"

#define BMP_BF_TYPE 0x4D42
#define BMP_BF_RESERVED 0
#define BMP_BI_PLANES 1
#define BMP_BI_SIZE 40
#define BMP_FILE_HEADER_SIZE 14
#define BMP_BI_BIT_COUNT 24
#define BMP_BI_COMPRESSION 0
#define BMP_BI_X_PELS_PER_METER 0
#define BMP_BI_Y_PELS_PER_METER 0
#define BMP_BI_CLR_USED 0
#define BMP_BI_CLR_IMPORTANT 0

#define BMP_HEADER_SIZE sizeof(struct bmp_header)

static bool read_header( FILE** in, struct bmp_header *header) {
    if(!file_read(header, BMP_HEADER_SIZE, 1, *in, 0)) {return false;}
    fseek(*in, header->bOffBits, SEEK_SET);

    return true;
}

static size_t padding_calculate(const struct image* const img) {
    size_t padding = 0;
    if (img->width % 4 != 0) padding = 4 - (size_t) ((img->width * sizeof(struct pixel)) % 4);
    return padding;
}

size_t calculate_image_size(const struct image* const img){
    return img->width + padding_calculate(img) * img->height;
}

size_t calc_file_size(const struct image* const img) {
    return img->width * img->height * sizeof(struct pixel) + padding_calculate(img) * img->height + sizeof(struct bmp_header);
}


static bool read_img(FILE* in, const struct image* img) {
	size_t padding = padding_calculate(img);

	struct pixel *start = img->data;

	for (
		struct pixel *row = img->data;
		row - start < img->width * img->height;
		row += img->width
	) {
		file_read(row, sizeof(struct pixel), img->width, in, padding);
	}

	return true;
}



enum read_status from_bmp( FILE* in, struct image* img ) {
    struct bmp_header header = {0};
    if (!read_header(&in, &header)){
        return READ_INVALID_HEADER;
    }

    *img = image_create(header.biWidth, header.biHeight);

    if (!img->data) {
        return IMAGE_DATA_INIT_ERROR;
    }

    if (!read_img(in, img)){
        image_delete(*img);
        return READ_INVALID_BITS;
    }

    return READ_OK;

}

struct bmp_header create_bmp_header(const struct image* const img) {
    return (struct bmp_header) {
        .bfType = BMP_BF_TYPE, 
        .bfileSize = calc_file_size(img),
        .bfReserved = BMP_BF_RESERVED, 
        .bOffBits = sizeof(struct bmp_header),
        .biSize = BMP_BI_SIZE,
        .biWidth = img->width,
        .biHeight = img->height,
        .biPlanes = BMP_BI_PLANES,
	    .biBitCount = BMP_BI_BIT_COUNT,
	    .biCompression = BMP_BI_COMPRESSION,
	    .biSizeImage = calculate_image_size(img),
	    .biXPelsPerMeter = BMP_BI_X_PELS_PER_METER,
	    .biYPelsPerMeter = BMP_BI_Y_PELS_PER_METER,
	    .biClrUsed = BMP_BI_CLR_USED,
	    .biClrImportant = BMP_BI_CLR_IMPORTANT
    };
}


enum write_status to_bmp( FILE* out, struct image const* img ) {
	struct bmp_header header = create_bmp_header(img);
	size_t padding = padding_calculate(img);

	if (!file_write(&header, BMP_HEADER_SIZE, 1, out, 0)) {
		return WRITE_ERROR;
	}

	for (size_t row_number = 0; row_number < img->height; ++row_number) {
		const struct pixel *current_row = img->data + row_number * img->width;

		if (!file_write(current_row, sizeof(struct pixel), img->width, out, padding)) {
			return WRITE_ERROR;
		}
	}

	return WRITE_OK;
}


