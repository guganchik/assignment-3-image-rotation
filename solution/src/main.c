#include "bmp.h"
#include "rotate.h"

int main( int argc, char** argv ) {

    if (argc < 3) {
        fprintf(stderr, "Invalid arguments. Expected: %s input.bmp output.bmp", argv[0]);
        return -1;
    }

    FILE* in = NULL;
    FILE* out = NULL;
    file_open_read(&in , argv[1]);
    if (!in) return -1;
    file_open_write(&out , argv[2]);
    if (!out) {
        fclose(in);
        return -1;
    }

    struct image img;


    switch (from_bmp(in, &img)) {
    case READ_OK:
        break;
    case READ_INVALID_HEADER:
        fprintf(stderr, "Couldn't read image header from '%s'\n", argv[1]);
        fclose(in);
        fclose(out);
        return -1;
    case READ_INVALID_BITS:
        fprintf(stderr, "Couldn't read image data from '%s'\n", argv[1]);
        fclose(in);
        fclose(out);
        return -1;
    case IMAGE_DATA_INIT_ERROR:
        fprintf(stderr, "Source image data initialization error");
        fclose(in);
        fclose(out);
        return -1;
    }
    struct image rotated = image_create(img.height, img.width);
    if (!rotated.data) {
        fprintf(stderr, "Rotated image data initialization error");
        fclose(in);
        fclose(out);
        return -1;
    }

    image_rotate(&rotated, &img);

    image_delete(img);

    switch (to_bmp(out, &rotated)) {
    case WRITE_OK:
        break;
    case WRITE_ERROR:
        fprintf(stderr, "Couldn't save transformed image to '%s'\n", argv[2]);
        image_delete(rotated);
        fclose(in);
        fclose(out);
        return -1;
    }

    image_delete(rotated);

    if ( fclose(in)!=0 || fclose(out)!=0 ) return -1;

    return 0;
}
